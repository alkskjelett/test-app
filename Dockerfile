FROM php:8.2-fpm

RUN apt-get update && apt-get install -y \
    nginx \
    libpq-dev \
    unzip \
    && docker-php-ext-install pdo pdo_mysql

#RUN chmod 777 ./web/assets
RUN chown www-data:www-data ./web/assets

RUN curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer

WORKDIR /app